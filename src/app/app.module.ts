import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

/*import { LoginModule } from './modules/login/login.module';*/
import { panelService } from './modules/panel/panel.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActiveRouteGuard } from './modules/services/activate-route-guard';
import { AppComponent } from './app.component';

import { routing } from './app.routing';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    /*LoginModule,*/
    BrowserAnimationsModule,
    routing
  ],
  providers: [
    panelService,
    ActiveRouteGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
