import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { ActiveRouteGuard } from './modules/services/activate-route-guard';

export const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full', canActivate: [ActiveRouteGuard]},
  { path: 'login', loadChildren: './modules/login/login.module#LoginModule', canActivate: [ActiveRouteGuard] },
  { path: 'register', loadChildren: './modules/register/register.module#RegisterModule', canActivate: [ActiveRouteGuard] },
  { path: 'panel', loadChildren: './modules/panel/panel.module#PanelModule'}

];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: false });
