import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

@Injectable()

export class appService{
	constructor(protected http:Http){

	}

	protected addTokenToHeader() {
        // create authorization header with jwt token
        let userDetail = JSON.parse(localStorage.getItem('userDetail'));
        if (userDetail && userDetail.token) {  
            let headers = new Headers({ 'Authorization': 'bearer '+userDetail.token });
            return new RequestOptions({ headers: headers });
        }
    }
	
	protected updateToken(data:Response){
        var token = data.headers.get('access-token');
	}
	
	protected handleError(err:Response):any{
		if(typeof err['status'] != "undefined" && err['status'] == 401){
			localStorage.removeItem('userDetail');
            window.location.href="/";
		}
	}
	
}