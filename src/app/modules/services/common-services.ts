import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';

import { AppSettings } from './../../constants';
import { appService } from './../../app.service';
@Injectable()

export class commonService extends appService{
	constructor(protected _http:Http){
		super(_http);
	}

	convertDateToTimestamp(val:string):string{
		var arrVal = val.split("/");
		var newDateStr = arrVal[2]+"-"+arrVal[1]+"-"+arrVal[0];
		var timestamp = new Date(newDateStr).getTime().toString();
		return timestamp;
	}

	convertTimestampToDate(val:string):string{
		var timestamp = parseInt(val);
		var d = new Date(timestamp);
		var newDate = this.padNumber(d.getDate()) + '/' + this.padNumber((d.getMonth()+1)) + '/' + d.getFullYear();
		return newDate;
	}

	padNumber(d) {
	    return (d < 10) ? '0' + d.toString() : d.toString();
	}
}