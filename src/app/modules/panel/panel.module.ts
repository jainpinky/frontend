import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { panelComponent } from './panel.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { MainSideComponent } from './main-side/main-side.component';
import { FooterComponent } from './footer/footer.component';
import { ControlSidebarComponent } from './control-sidebar/control-sidebar.component';

import { ActiveRouteGuard } from './../services/activate-route-guard';
import { DeactiveRouteGuard } from './../services/deactivate-route-guard';

import { ConfirmComponent } from "./../popup-component/confirm.component";

import { panelService } from './panel.service';
 
import { routing } from './panel.routing';

declare let require: any;

@NgModule({
	imports:[
		CommonModule,
		routing
	],
	declarations:[
		panelComponent,
		MainHeaderComponent,
		MainSideComponent,
		FooterComponent,
		ControlSidebarComponent,
		ConfirmComponent
	],
	entryComponents: [
		ConfirmComponent,
	],
	providers:[
		ActiveRouteGuard,
		DeactiveRouteGuard,
		panelService
	]
})

export class PanelModule{}