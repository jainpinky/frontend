import { Component, OnInit } from '@angular/core';

import { panelService } from './../panel.service';
import { AppSettings } from './../../../constants';
@Component({
    selector: '[panel-main-side]',
    templateUrl: './main-side.component.html',
    styleUrls: ['./main-side.component.css']
})
export class MainSideComponent implements OnInit {
    
    public loggedData:any;
    constructor(private _panelService:panelService){
        
        this.loggedData = this._panelService.getSession();
        if(typeof this.loggedData.profile_image == "undefined"){
            this.loggedData.profile_image = "./assets/img/noimage.png";    
        }
    }
    ngOnInit() {
    
    }

}
