import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { appService } from './../../../app.service';
import { AppSettings } from './../../../constants';

@Injectable()

export class dashboardService extends appService {
	constructor(private _http:Http){
		super(_http);
	}
	
	getDetail():any{
		return this._http.get(AppSettings.PROFILE_DETAIL, this.addTokenToHeader()).do(this.updateToken).map((response: Response) => response.json()).catch(this.handleError);
	}

}