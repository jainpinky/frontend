import { Component, OnInit } from '@angular/core';
import { panelService } from './../panel.service';
import { dashboardService } from './dashboard.service';
declare var $: any;

@Component({
	selector:'dashboard-app',
	templateUrl:'./dashboard.html'
})

export class dashboardComponent implements OnInit {
	
	public loggedData:any;
	public stats = {};
	ngOnInit() {
		
	}
	constructor(private _panelService:panelService, private _dashboardService:dashboardService){
		//this.loggedData = localStorage.getItem('userDetail');
		this._panelService.setSession(JSON.parse(localStorage.getItem('userDetail')));
		this.loggedData = this._panelService.getSession();
		this.getUserDetail();
	}
	
	getUserDetail(){
		this._dashboardService.getDetail().subscribe(response => {
			console.log("Response from system");
			console.log(response);
		});
	}
}