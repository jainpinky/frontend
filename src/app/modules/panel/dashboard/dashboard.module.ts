import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { dashboardComponent } from './dashboard.component';
import { dashboardService } from './dashboard.service';
import { routing } from './dashboard.routing';

@NgModule({
	imports:[
		CommonModule,
		routing
	],
	declarations:[
		dashboardComponent
	],
	providers:[
		dashboardService
	]
})

export class DashboardModule{}