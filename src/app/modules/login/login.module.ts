import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { loginService } from './login.service';


import { loginComponent } from './login.component';

import { routing } from './login.routes';

@NgModule({
	imports:[
		FormsModule,
		ReactiveFormsModule,
		CommonModule,
		routing
	],
	declarations:[
		loginComponent
	],
	providers:[
		loginService
	]
})

export class LoginModule{}