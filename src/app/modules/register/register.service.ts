import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';

import { AppSettings } from './../../constants';
@Injectable()

export class registerService{
	constructor(private http:Http){

	}

	signup(obj:Object):any{
		return this.http.post(AppSettings.ADD_USER, obj).map((response: Response) => response.json());
	}
}